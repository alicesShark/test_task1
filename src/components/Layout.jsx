import React from 'react';
import {Outlet} from 'react-router-dom';
import Header from './header/Header';
import Tabs from './tabs/Tabs';
import {Footer} from './Footer';


const Layout = () => {
    return (
        <div>
            <Header />
            <Tabs />
            <Outlet />
            <Footer />
        </div>
    );
};

export default Layout;