import React from 'react';
import { Link } from 'react-router-dom';

import cl from './Header.module.css';

import Phone from '../svg/Phone';
import Logo from '../svg/Logo'

const Header = () => {
    return (
        <div className={cl.header}>
            <div className={cl.header__container}>
                <Link to="/" className={cl.header__logo}>
                    <Logo />
                </Link>
                <a
                    className={cl.header__link}
                    href='tel:+7 (495) 495-49-54'
                >
                    +7 (495) 495-49-54
                </a>
                <a
                    className={cl.header__linkMobile}
                    href='tel:+7 (495) 495-49-54'
                >
                    <Phone />
                </a>
            </div>
        </div>
    );
};

export default Header;