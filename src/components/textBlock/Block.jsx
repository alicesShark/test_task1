import React from 'react';
import cl from './Block.module.css';

const Block = ({block}) => {
    return (
        <div className={cl.block}>
            <div className={cl.block__wrap}>
                <h2 className={cl.block__title}>{block.title}</h2>
                {
                    block.text.map(i => (
                        <p className={cl.block__text}>{i}</p>
                    ))
                }
            </div>
            <div className={cl.block__cover}>
                <img src={block.img.src} alt={block.img.alt} />
            </div>
        </div>
    );
};

export default Block;