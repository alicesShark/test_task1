import React from 'react';
import cl from './Tabs.module.css';
import {Tab} from './Tab';

const Tabs = () => {
    return (
        <div className={cl.tabs}>
            <Tab className={cl.tabs__link} to='/'>Home</Tab>
            <Tab className={cl.tabs__link} to='/slider'>Slider</Tab>
        </div>
    );
};

export default Tabs;