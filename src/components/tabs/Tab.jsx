import React from 'react';
import { Link, useMatch } from 'react-router-dom';

const Tab = ({children, to, ...props}) => {
    const match = useMatch(to);

    return (
        <Link
            to={to}
            style={{
                borderBottomColor: match ? '#141414 ' : 'transparent',
            }}
            {...props}
        >
            {children}
        </Link>
    )
}

export {Tab};