import React from 'react';

const footer = {
    boxSizing: 'border-box',
    width: '100%',
    height: 200,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
    padding: '20px 40px',
    background: '#141414',
    fontFamily: 'Roboto, sans-serif',
    fontWeight: 400,
    fontSize: 14,
    color: '#FFFFFF'
}

export const Footer = () => {return <div style={footer}>© TEST, 1022–2022</div>};