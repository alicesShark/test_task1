import React from 'react';
import Slider from 'react-slick';
import cl from './Slider.module.css';

const NextArrow = ({onClick}) => {
    return (
        <div className={cl.sliderButton}
            onClick={onClick}
            style={{
                left: 'auto',
                right: 'calc(50% - 120px)',
                transform: 'rotate(45deg)'
            }}
        />
    );
}

const PrevArrow = ({onClick}) => {
    return (
        <div className={cl.sliderButton}
            onClick={onClick}
            style={{
                left: 'calc(50% - 120px)',
                transform: 'rotate(-135deg)',
            }}
        />
    );
}

const SliderComponent = ({slides}) => {
    const settings = {
        className: `${cl.slider}`,
        infinite: true,
        centerPadding: '40px',
        slidesToShow: 5,
        swipeToSlide: true,
        slidesToScroll: 1,
        nextArrow: <NextArrow />,
        prevArrow: <PrevArrow />,
        responsive: [
            {
                breakpoint: 1819,
                settings: {
                    slidesToShow: 4,
                    infinite: true,
                }
            },
            {
                breakpoint: 1439,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 899,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 639,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    };

    return (
        <div className={cl.sliderWrap}>
            <Slider {...settings}>
                {slides.map((slide) => (
                    <div className={cl.slide}>
                        <img src={slide.img.src} alt={slide.img.alt} />
                        {slide.desc ? <div className={cl.slideDesc}>{slide.desc}</div> : ''}
                    </div>
                ))}
            </Slider>

            <div className={cl.shadowLeft} />
            <div className={cl.shadowRight} />
        </div>
    );
};

export default SliderComponent;