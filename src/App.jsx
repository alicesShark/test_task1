import React from 'react';
import {Routes, Route} from 'react-router-dom';

import HomePage from './pages/HomePage';
import SliderPage from './pages/SliderPage';
import NotFoundPage from './pages/NotFoundPage';
import Layout from './components/Layout';

import Slider from './mocks/Slider';
import Block from './mocks/Block';

function App() {
  return (
    <div className="App">
        <Routes>
            <Route path='/' element={<Layout />}>
                <Route index element={<HomePage block={Block} />} />
                <Route path='slider' element={<SliderPage slides={Slider} />} />
                <Route path='*' element={<NotFoundPage />} />
            </Route>
        </Routes>
    </div>
  );
}

export default App;
