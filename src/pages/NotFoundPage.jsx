import React from 'react';

const NotFoundPage = () => {
    return (
        <div className='container'>
            <h1 style={{textTransform: 'uppercase', textAlign: 'center'}}>
                There's
                <i style={{textTransform: 'lowercase'}}> nothing </i>
                here...
            </h1>
        </div>
    );
};

export default NotFoundPage;