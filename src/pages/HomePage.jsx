import React from 'react';
import Block from "../components/textBlock/Block";

const HomePage = ({block}) => {
    return (
        <div
            className='container'
            style={{maxWidth: '1440px'}}
        >
            <h1 style={{textTransform: 'uppercase'}}>
                Lorem ipsum dolor
                <i style={{textTransform: 'lowercase'}}> sit </i>
                amet
            </h1>

            {block.map((item) => (
                <Block block={item}/>
            ))}
        </div>
    );
};

export default HomePage;