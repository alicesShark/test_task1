import React from 'react';
import SliderComponent from '../components/slider/SliderComponent';

const SliderPage = ({slides}) => {
    return (
        <div className='container' style={{padding: '110px 0'}}>
            <h1 style={{
                    textTransform: 'uppercase',
                    textAlign: 'center'
                }}
            >
                Lorem ipsum dolor
                <i style={{textTransform: 'lowercase'}}> sit </i>
                amet
            </h1>

            <SliderComponent slides={slides} />
        </div>
    );
};

export default SliderPage;
