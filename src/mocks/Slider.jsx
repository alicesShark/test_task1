const Slider = [
    {
        id: '1',
        img: {
            src: '/assets/slide_1.png',
            alt: 'slide',
        }
    },
    {
        id: '2',
        img: {
            src: '/assets/slide_2.png',
            alt: 'slide',
        }
    },
    {
        id: '3',
        img: {
            src: '/assets/slide_3.png',
            alt: 'slide',
        }
    },
    {
        id: '4',
        img: {
            src: '/assets/slide_4.png',
            alt: 'slide',
        },
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        descDir: 'bottom',
    },
    {
        id: '5',
        img: {
            src: '/assets/slide_5.png',
            alt: 'slide',
        },
        desc: 'Lorem ipsum dolor sit amet',
        descDir: 'right',
    },
    {
        id: '3',
        img: {
            src: '/assets/slide_3.png',
            alt: 'slide',
        }
    },
    {
        id: '4',
        img: {
            src: '/assets/slide_4.png',
            alt: 'slide',
        }
    },
]

export default Slider;